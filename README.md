# merehead

If you want to edit config you need to run

$ php artisan vendor:publish --provider="merehead/module-connector" --tag=config 

$ php artisan vendor:publish

So config-file will be moved to /config/module-connector.phpnd can be edited as you want and changes will not be lost after composer update.