<?php

namespace Providers;

use Modules\TradeModuleService;


class TradeModuleServiceProvider extends ModuleServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();

        \App::bind('trademodule', function()
        {
            return new TradeModuleService();
        });
    }
}
