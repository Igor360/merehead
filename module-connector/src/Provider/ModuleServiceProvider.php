<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 7/26/18
 * Time: 12:46 PM
 */

namespace Providers;

use Config;

class ModuleServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const PACKAGE_ALIAS = 'module-connector';

    /**
     * Address trade module
     * @var
     */
    protected $tradeModuleDns;


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->mergeConfigFrom(__DIR__ . '/../../config/module-connector.php', 'module-connector');
    }


    /**
     * @throws \Exception
     */
    public function boot() {
        $this->publishes([__DIR__ . '/../../config/module-connector.php' => config_path('module-connector.php')], 'config');
        foreach (config('phpconsole') as $option => $value) {
            $this->setOption($option, $value);
        }
        $this->initPhpConsole();
    }

    /**
     * Setting class option
     * @param $name
     * @param $value
     * @throws \Exception
     */
    protected function setOption($name, $value) {
        if (!property_exists($this, $name)) {
            throw new \Exception('Unknown property "' . $name . '" in ' . static::PACKAGE_ALIAS . ' package config');
        }
        $this->$name = $value;
    }
}