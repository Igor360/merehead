<?php

namespace Modules;

/**
 * This class using for sending data to server
 * Class TradeModuleService
 * @package Modules
 */
class TradeModuleService extends TradeConnectionModule
{

    public function ping() {
        $msg = [
            'command' => 'ping',
        ];
        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function getAssets()
    {
        $msg = [
            'command' => 'get_assets',
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function getAssetsPairs()
    {
        $msg = [
            'command' => 'get_assets_pairs',
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);
        return $reply;
    }

    public function getOrdersBook(string $pair)
    {
        $msg = [
            'command' => 'get_orders_book',
            'data' => [
                'pair' => $pair,
            ],
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function getTrades(string $pair)
    {
        $msg = [
            'command' => 'get_trades',
            'data' => [
                'pair' => $pair,
            ],
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }




    public function createAccount(int $user_id){
        $msg = [
            'command' => 'create_account',
            'data' => [
                'account_id' => $user_id
            ]
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function getBalances(int $accountId){
        $msg = [
            'command' => 'get_balances',
            'data' => [
                'account_id' => $accountId,
            ],
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function increaseBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => 'increase_balance',
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function freezeBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => 'freeze_balance',
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function decreaseBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => 'decrease_balance',
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function getOrders(int $accountId, string $pair = null, string $status = null, int $current_page = 0, int $per_page = 15)
    {
        $msg = [
            'command' => 'get_orders',
            'data' => [
                'account_id' => $accountId,
                'pair' => $pair,
                'status' => $status,
                'current_page' => $current_page,
                'per_page' => $per_page
            ],
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function getTransactions(int $accountId)
    {
        $msg = [
            'command' => 'get_transactions',
            'data' => [
                'account_id' => $accountId,
            ],
        ];

        $this->requester->send(json_encode($msg));
        $reply = json_decode($this->requester->recv(), 1);

        return $reply;
    }

    public function calculateLimitOrder(int $accountId, string $type, string $pair, float $price, float $quantity)
    {
        $msg = [
            'command' => 'calculate_limit_order',
            'data' => [
                'account_id' => $accountId,
                'type' => $type,
                'pair' => $pair,
                'price' => $price,
                'quantity' => $quantity,
            ],
        ];

        return $this->makeCall($msg);
    }

    public function calculateMarketOrder(int $accountId, string $type, string $pair, float $quantity)
    {
        $msg = [
            'command' => 'calculate_market_order',
            'data' => [
                'account_id' => $accountId,
                'type' => $type,
                'pair' => $pair,
                'quantity' => $quantity,
            ],
        ];

        return $this->makeCall($msg);
    }

    public function createLimitOrder(int $accountId, string $type, string $pair, float $price, float $quantity)
    {
        $msg = [
            'command' => 'create_limit_order',
            'data' => [
                'account_id' => $accountId,
                'type' => $type,
                'pair' => $pair,
                'price' => $price,
                'quantity' => $quantity,
            ],
        ];

        return $this->makeCall($msg);
    }

    public function createMarketOrder( $accountId, $type, $pair, $quantity)
    {
        $msg = [
            'command' => 'create_market_order',
            'data' => [
                'account_id' => $accountId,
                'type' => $type,
                'pair' => $pair,
                'quantity' => $quantity,
            ],
        ];

        return $this->makeCall($msg);
    }

    public function cancelOrder($user_id, $orderId)
    {
        $msg = [
            'command' => 'cancel_order',
            'data' => [
                'account_id' => $user_id,
                'order_id' => $orderId,
            ],
        ];
        return $this->makeCall($msg);
    }


    public function getHotWalletBalance()
    {

        $msg = [
            'command' => 'ger_hot_wallet_balance',
            'data' => [ ],
        ];

        return $this->makeCall($msg);
    }

    public function getOrdersTransactions($user_id){
        $msg = [
            'command' => 'get_orders_transactions',
            'data' => [
                'account_id' => $user_id
            ],
        ];

        return $this->makeCall($msg);
    }

    public function createUserFee($user_id){
        $msg = [
            'command' => 'create_user_fee',
            'data' => [
                'account_id' => $user_id
            ]
        ];
        return $this->makeCall($msg);
    }


    public function getAssetsPairsCodes()
    {
        $msg = [
            'command' => 'get_assets_pairs_codes'
        ];
        return $this->makeCall($msg);
    }
}