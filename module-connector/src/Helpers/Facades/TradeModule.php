<?php

namespace Helpers\Facades;


/**
 * Facade who using for connecting to trade module
 * Class TradeModule
 * @package Helpers\Facades
 */
class TradeModule extends \Facade
{


    protected static function getFacadeAccessor()
    {
        return 'trademodule';
    }

}